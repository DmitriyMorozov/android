package com.example.myapplication.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication.Myapplication
import com.example.myapplication.database.Entity.Person
import com.example.myapplication.database.Entity.PersonAvatar

class PersonDB {
    companion object
    {
        private var instance: PersonDatabase? = null
        fun getInstance(): PersonDatabase
        {
            if(instance == null){
                instance = Room.databaseBuilder(
                    Myapplication.applicationContext(),
                    PersonDatabase::class.java,
                    "persons"
                ).build()
            }
            return instance!!
        }
    }
}
@Database(
    entities =
    [
        Person::class,
        PersonAvatar::class
    ],
    version = 2
)
abstract class PersonDatabase: RoomDatabase()
{
    abstract fun personDAO(): PersonDAO
}
