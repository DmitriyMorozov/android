package com.example.myapplication

import android.app.Application
import android.content.Context

class Myapplication : Application() {
    init {
        instance = this
    }
    companion object{
        private var instance: Myapplication? = null
        fun applicationContext(): Context
        {
            return instance!!.applicationContext
        }
    }
}
